/*
--------------------------------------------------------------------
pewpew
--------------------------------------------------------------------
*/
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "Chat.h"

#define ITEM_NEEDED 31496 //Странный фолиант
#define MONEY_LVL_2 1500000 //Gold
#define MONEY_LVL_1 1000000

class npc_archaelogy : public CreatureScript
{
public:
	npc_archaelogy() : CreatureScript("npc_archaelogy") { }
	int selected_enchant;

	bool OnGossipHello(Player* player, Creature* creature)
	{
		ShowMainMenu(player, creature);
		return true;
	}

	void ShowMainMenu(Player* player, Creature* creature)
	{
        player->PlayerTalkClass->ClearMenus();

            if (!player->HasArchaelogySkill())
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Изучить археологию 1ый уровень - 100 золота", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);

            if (player->HasArchaelogySkill())
            {

                if (player->GetArchaelogySkill()==150 && player->GetArchaelogyLvl()==1)
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Изучить второй уровень - 250 золота + 150 [Странный фолиант]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
            }

        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Узнать про археологию...", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);

		// Здесь можно сделать проверку на уровень скилла - реализуется вот так:
		//if (player->HasSkill(SKILL_ENCHANTING) && player->GetSkillValue(SKILL_ENCHANTING) >= 450)
			//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Enchant Rings", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 14);

		player->SEND_GOSSIP_MENU(1, creature->GetGUID());
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		switch (action)
		{
		case GOSSIP_ACTION_INFO_DEF + 1:
        {
            if (player->HasEnoughMoney(MONEY_LVL_1))
                {
                  player->ModifyMoney(-MONEY_LVL_1);
                  player->GetSession()->SendNotification("Вы изучили археологию. Поздравляем!");
                  player->LearnArchaelogySkill(1,1);
                  ChatHandler(player->GetSession()).PSendSysMessage("Навык археологии повышен, сейчас: 1");
                }
            else
            ChatHandler(player->GetSession()).PSendSysMessage("Недостаточно золота.");
			break;
         }
        case GOSSIP_ACTION_INFO_DEF + 2:
            if (player->HasItemCount(ITEM_NEEDED,150,true) && player->HasEnoughMoney(MONEY_LVL_2))
               {
                    player->ModifyMoney(-MONEY_LVL_2);
                    player->DestroyItemCount(ITEM_NEEDED, 150, true);
                    player->GetSession()->SendNotification("Второй уровень археологии изучен! Поздравляем");
                    player->LearnArchaelogySkill(150,2);
                    ChatHandler(player->GetSession()).PSendSysMessage("Навык археологии повышен, сейчас: 150");
               }
            else
                ChatHandler(player->GetSession()).PSendSysMessage("У вас недостаточно предметов или золота.");
			break;
		default:
			player->CLOSE_GOSSIP_MENU();
			break;
		}
		return true;
	}
};

void AddSC_npc_archaelogy()
{
	new npc_archaelogy();
}
